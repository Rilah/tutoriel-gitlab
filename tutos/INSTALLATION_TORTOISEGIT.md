Installation de TortoiseGit sous Windows
=============

ToroiseGit est un shell Windows permettant l'utilisation de Git. Il facilite l'utilisation de git sous Windows en mode clic bouton. Concrètement il permet d'avoir dans l'explorateur 
Windows des infos sur les fichiers git, de 
nouvelles options git pour le clic 
droit 
sur un fichier et des interfaces facilitant l'utilisation de git.

On supposera dans ce tutoriel que git.exe est installé. Sinon suivre ce 
[tutoriel](https://gitlab.com/DREES/tutoriels/blob/master/tutos/INSTALLATION_GIT_EXE.md).

Attention
------------

TortoiseGit semble nécessiter les droits administrateur pour son installation. 

TortoiseGit modifie 
l'explorateur Windows en y ajoutant de nouveaux icones associés aux fichiers et 
dossiers git. C'est probablement la cause du blocage.

Installation
---------------

Suivre ce [lien](https://tortoisegit.org/download/).

![1_download](https://gitlab.com/DREES/tutoriels/raw/master/img/tortoisegit/1_download.PNG)

Cliquer sur le lien de téléchargment de TortoiseGit. En théorie le 64-bit. Ensuite double cliquer sur 
l'installeur TortoiseGit qui est normalement dans votre dossier de Téléchargements.

![2_executer](https://gitlab.com/DREES/tutoriels/raw/master/img/tortoisegit/2_executer.PNG)

Exécuter le programme.

![3_next](https://gitlab.com/DREES/tutoriels/raw/master/img/tortoisegit/3_next.PNG)

Cliquer sur "Next".

![4_next](https://gitlab.com/DREES/tutoriels/raw/master/img/tortoisegit/4_next.PNG)

Cliquer sur "Next".

![5_ssh](https://gitlab.com/DREES/tutoriels/raw/master/img/tortoisegit/5_ssh.PNG)

On conseille ici "TortoiseGitPlink". Le choix n'est pas définitif et modifiable dans les paramètres de 
TortoiseGit.

Cliquer sur "Next".

![6_location](https://gitlab.com/DREES/tutoriels/raw/master/img/tortoisegit/6_location.PNG)

Choisir la location appropriée via le bouton "Browse". Il est conseillé d'utiliser un dossier local 
surlequel vous avez les droits
du type ```C:\Users\votre.nom\AppData\Local\TortoiseGit\```. Une fois 
choisi cliquer sur "Next".

![7_browse](https://gitlab.com/DREES/tutoriels/raw/master/img/tortoisegit/7_browse.PNG)

Ici l'exploreur pour trouver le chemin approprié.

![8_install](https://gitlab.com/DREES/tutoriels/raw/master/img/tortoisegit/8_install.PNG)

Pour finir cliquer sur "Install".

![9_congratz](https://gitlab.com/DREES/tutoriels/raw/master/img/tortoisegit/9_congratz.PNG)

L'installation est terminée. On va passer à la configaration avec le "first start wizard". Ce dernier ce 
lance automatiquement en cliquant sur "Finish".

Configuration au premier lancement
-----------------------------

On passe désormais aux premières configurations. Si le wizard ne s'est pas lancé, il est possible de le 
lancer en cherchant "TortoiseGit" dans le menu Démarrer.

![10_wiz](https://gitlab.com/DREES/tutoriels/raw/master/img/tortoisegit/10_wiz.PNG)

On va d'abord télécharger le package de langue française. Pour ça on retourne sur ce 
[lien](https://tortoisegit.org/download/).

![11_language](https://gitlab.com/DREES/tutoriels/raw/master/img/tortoisegit/11_language.PNG)

Dérouler vers le bas jusqu'à tomber sur le package français et cliquer sur "Setup". Ensuite double cliquer 
sur le package de langue qui se trouve normalement dans votre dossier Téléchargements.

![12_fr_next](https://gitlab.com/DREES/tutoriels/raw/master/img/tortoisegit/12_fr_next.PNG)

Cliquer sur "Next".

![13_fr_congratz](https://gitlab.com/DREES/tutoriels/raw/master/img/tortoisegit/13_fr_congratz.PNG)

Cliquer sur "Terminer".

![14_wiz_fr](https://gitlab.com/DREES/tutoriels/raw/master/img/tortoisegit/14_wiz_fr.PNG)

En cliquant sur "Refresh", le package français est disponible dans le menu déroulant. Le choisir et cliquer 
sur "Suivant".

![15_suivant](https://gitlab.com/DREES/tutoriels/raw/master/img/tortoisegit/15_suivant.PNG)

Cliquer sur "Suivant".

![16_install_git](https://gitlab.com/DREES/tutoriels/raw/master/img/tortoisegit/16_install_git.PNG)

Si le git.exe est bien installé le chemin vers ce dernier devrait être automatiquement indiqué. Sinon 
il faut l'installer via ce 
[tutoriel](https://gitlab.com/DREES/tutoriels/blob/master/tutos/INSTALLATION_GIT_EXE.md). 
Si git.exe est bien installé, indiquer le bon chemin, puis cliquer 
sur "Suivant".

![17_nom_mail](https://gitlab.com/DREES/tutoriels/raw/master/img/tortoisegit/17_nom_mail.PNG)

Indiquer son nom et mail tels qu'ils apparaîtront sous git. Préférer votre email du type @sante.gouv.fr, 
celui que vous avez utilisé pour votre compte GitLab normalement. Cliquer sur "Suivant".

![18_a_generation](https://gitlab.com/DREES/tutoriels/raw/master/img/tortoisegit/18_a_generation.PNG)

Cliquer sur "Terminer". La génération de clef ayant été déjà faite grâce à git.exe via ce 
[tutoriel](https://gitlab.com/DREES/tutoriels/blob/master/tutos/INSTALLATION_GIT_EXE.md).

![26_tortoise_is_here](https://gitlab.com/DREES/tutoriels/raw/master/img/tortoisegit/26_tortoise_is_here.PNG)

Désormais en faisant un clic droit sur un fichier ou dossier de nouvelles options devraient être disponible 
sous "TortoiseGit". De plus de nouveaux icônes rendent compte de l'état des projets git.

Il vous faut désormais ajouter la clé publique RSA que vous venez de créer à votre compte GitLab en suivant 
ce [tutoriel](https://gitlab.com/DREES/tutoriels/blob/master/tutos/AJOUT_CLE_PUB_GITLAB.md).
